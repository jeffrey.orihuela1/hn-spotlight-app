# Hn Spotlight App

![](ui-view.png)

### Steps to run Hn Spotlight

1. From the frontend folder, type `$ docker build -t frontimage .`
2. From the server folder, type `$ docker build -t backimage .`
3. From the root folder, type `$ docker-compose up -d`
4. Open your browse and go to `localhost:3000`

### Considerations

The app uses the followings ports: 3000, 3001, 27017
