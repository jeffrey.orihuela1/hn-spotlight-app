export interface Hit {
    id: string,
    date: string;
    title: string;
    author: string;
    url: string;
    deleted: boolean;
}