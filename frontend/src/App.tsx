import React from 'react';
import './App.css';
import Header from './components/header/Header';
import Table from './components/table/Table';

function App() {
  return (
    <div>
      <Header></Header>
      <Table></Table>
    </div>
  )
}

export default App;
