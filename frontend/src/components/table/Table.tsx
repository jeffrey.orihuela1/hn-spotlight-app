import React, { useState, useEffect } from 'react';
import './Table.css';
import { Hit } from '../../model/Hit';
import Row from './row/Row';

const Table = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [hits, setHits] = useState<Hit[]>([]);

    useEffect(() => {
        getHits();
    }, []);

    const getHits = async () => {
        try {
            const response = await fetch("http://localhost:3001/hit")    
            const hitsArray = await response.json();
            console.log(hitsArray);
            setHits(hitsArray);
        } catch (error) {
            console.log(error);
        }
        setIsLoading(false);
    }

    const findHits = async () => {
        setIsLoading(true);
        try {
            const response = await fetch("http://localhost:3001/hit/find")    
            const hitsArray = await response.json();
            console.log(hitsArray);
            setHits(hitsArray);
        } catch (error) {
            console.log(error);
        }
        setIsLoading(false);
    }

    const deleteHit = async (id: string) => {
        setIsLoading(true);
        try {
            const response = await fetch("http://localhost:3001/hit/"+id, {method: 'DELETE'})
            if(response.ok) {
                setHits(hits.filter(hit => hit.id !== id));
            }
        } catch (error) {
            console.log(error);
        }
        setIsLoading(false);
    }

    return (
        isLoading ?
            <div>IS LOADING</div>
        :
            <div className="content">
                {
                    hits.length && hits.length !== 0
                    ? hits.map( (hit, index) => 
                        <Row hit={hit} id={index.toString()} deleteHit={deleteHit}></Row>)
                    :   <div>
                        <div>It seems there is no hacker new saved!</div>
                        <div>Please click <b onClick={() => findHits()}>here</b> to find hacker news.</div>
                        </div>
                }
            </div> 
    );
}

export default Table;