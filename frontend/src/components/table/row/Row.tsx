import React from 'react';
import './Row.css';
import { Hit } from '../../../model/Hit';
import DateFormat from '../../../utils/DateFormat';
import trash from '../../../assets/TRASH.png';

interface RowProps {
    hit: Hit,
    id: string,
    deleteHit: (id: string) => Promise<void>
}

const Row = ({hit, id, deleteHit}: RowProps) => {

    const openUrl = () => {
        window.open(hit.url, "_blank");
    }

    const onClickDelete = async () => {
        console.log(hit.id);
        await deleteHit(hit.id);
    }

    return <div className="Row" id={id} >
        <div className="inside-row" onClick={openUrl}>
            <div className="text-hit">
                <div className="title" >{hit.title}</div>
                <div className="author" >{hit.author}</div>
            </div>
            <div className="date">
                {DateFormat(hit.date)}
            </div>
        </div>
        <div className="trash-icon" onClick={onClickDelete}>
            <img src={trash} alt={"delete"} />
        </div>
    </div>
}

export default Row;