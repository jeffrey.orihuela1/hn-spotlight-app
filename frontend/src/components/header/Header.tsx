import React from 'react';
import logo from '../../assets/NEWS_2.png';
import './Header.css';

const Header = () => {
    return (
        <div className="Header">
            <div className="logo">
                <img src={logo} alt={"logo"} />
            </div>
            <div className="title">
                <div className="title-text">HN Spotlight</div>
                <div className="subtitle">The most trending news</div>
            </div>
            
        </div>
    );
}

export default Header;