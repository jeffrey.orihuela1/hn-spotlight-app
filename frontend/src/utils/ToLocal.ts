import moment from "moment";

const ToLocal = (date: string) => {
    return moment.utc(date).local().format('YYYY-MM-DD HH:mm:ss');
}

export default ToLocal;

