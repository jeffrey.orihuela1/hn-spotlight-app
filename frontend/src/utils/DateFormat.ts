import moment from "moment";
import ToLocal from "./ToLocal";

const DateFormat = (date: string) => {
    let dateLocal = ToLocal(date);
    let yesterday = moment().subtract(1, "day").startOf('day')

    if(moment(dateLocal).isSame(moment.now(), "day")) {
        return moment.utc(dateLocal).format('h a');
    } else if (moment(dateLocal).startOf('day').isSame(yesterday, "day")) {
        return "Yesterday";
    } else {
        return moment.utc(dateLocal).format("MMM D")
    }
}

export default DateFormat;

