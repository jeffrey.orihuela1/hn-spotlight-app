import {Document} from 'mongoose';

export interface Hit extends Document {
    date: string;
    title: string;
    author: string;
    url: string;
    deleted: boolean;
}