import { Controller, Get, Delete, Param, HttpService, Logger } from '@nestjs/common';
import { HitService } from './hit.service';
import { HitDTO } from './dto/hit.dto';

@Controller('hit')
export class HitController {
    constructor(private readonly hitService: HitService, private httpService: HttpService) {}

    @Get()
    async getAllHits() {
        const hits = await this.hitService.getHits();
        return hits;
    }

    @Delete(':id')
    async removeHit(@Param('id') id: string) {
        await this.hitService.deleteHit(id);
        return null;
    }

    @Get("find")
    async findHits() {
        const uniqueHits = await this.hitService.getHitsByUrl("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");
        
        let hitsPromises = uniqueHits.map(async (hit: any) => {
            const foundHit = await this.hitService.findByUrl(hit.story_url);
            if(!foundHit) {
                return await this.hitService.saveHit(
                    hit.story_url,
                    hit.created_at,
                    hit.title ?  hit.title : hit.story_title,
                    hit.author
                )
            }
        });

        const response = await Promise.all(hitsPromises);
        
        const filterResponse = response.filter(hit => hit !== undefined);
        
        const hitsDTO: HitDTO[] = filterResponse.map(hit => {
            return {id: hit.id, author: hit.author, date: hit.date, title: hit.title, url: hit.url};
        });

        return hitsDTO;
        
    }
    
}
