import * as mongoose from 'mongoose';

export const HitSchema = new mongoose.Schema({
    date: String,
    title: String,
    author: String,
    url: String,
    deleted: Boolean
})