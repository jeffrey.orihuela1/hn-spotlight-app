import { Injectable, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Hit } from './interface/hit.interface';

@Injectable()
export class HitService {
    constructor(@InjectModel('Hit') private readonly hitModel: Model<Hit>,private httpService: HttpService ) {}

    async deleteHit(id: string): Promise<any> {
        const deletedHit = await this.hitModel.findByIdAndUpdate(id, {
            deleted: true
        });

        return deletedHit;
    }

    async getHits() {
        const hits = await this.hitModel.find({deleted: false}).sort({date : 'desc'}).exec();
        return hits.map(hit => ({
            id: hit.id,
            title: hit.title,
            author: hit.author,
            url: hit.url,
            date: hit.date
        }))
    }

    async saveHit(url: string,
        date: string,
        title: string,
        author: string) {
        const savedHit = new this.hitModel({
            url: url,
            date: date,
            title: title,
            author: author,
            deleted: false
        });
        const result = await savedHit.save();
        return result;
    }

    async findByUrl(url: string) {
        const foundHit = await this.hitModel.find({url: url})
        return foundHit.length !== 0 ? true : false;
    }

    async getHitsByUrl(url: string) {
        let response = this.httpService.get(url);
        const responsePromise = await response.toPromise();
        const hits = responsePromise.data.hits;

        const hitsFiltered = hits.filter(hit => !!hit.story_url);

        const urlSet = Array.from(new Set(hitsFiltered.map(hit => hit.story_url)));

        const uniqueHits = Array.from(urlSet).map(url => {
            return hitsFiltered.find(hit => hit.story_url === url);
        })

        return uniqueHits;
    }
}
