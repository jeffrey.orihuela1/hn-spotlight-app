export class HitDTO{
    constructor( id: string, url: string, date: string, title: string, author: string) {}

    id: string;
    url: string;
    date: string;
    title: string;
    author: string;
}