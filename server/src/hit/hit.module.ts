import { Module, HttpModule } from '@nestjs/common';
import { HitService } from './hit.service';
import { HitSchema } from './schema/hit.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { HitController } from './hit.controller';

@Module({
  imports: [HttpModule, MongooseModule.forFeature([{ name: "Hit", schema: HitSchema }])],
  providers: [HitService],
  exports: [HitService],
  controllers: [HitController]
})
export class HitModule {}
