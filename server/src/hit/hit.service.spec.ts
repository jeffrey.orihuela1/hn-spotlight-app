import { Test, TestingModule } from '@nestjs/testing';
import { HitService } from './hit.service';
import { HttpModule } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';

describe('HitService', () => {
  let service: HitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HitService,
        {
          provide: getModelToken('Hit'),
          useValue: {

          }
        }
      ],
      imports: [HttpModule]
    }).compile();

    service = module.get<HitService>(HitService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
