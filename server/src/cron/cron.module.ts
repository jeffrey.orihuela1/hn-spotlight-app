import { Module } from '@nestjs/common';
import { CronService } from './cron.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HitSchema } from 'src/hit/schema/hit.schema';
import { HitModule } from '../hit/hit.module';

@Module({
    imports: [ MongooseModule.forFeature([{ name: 'Hit', schema: HitSchema }]), HitModule],
    providers: [CronService]
})
export class CronModule {}
