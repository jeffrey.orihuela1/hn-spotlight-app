import { Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Hit } from 'src/hit/interface/hit.interface';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HitService } from 'src/hit/hit.service';

@Injectable()
export class CronService {
    private readonly logger = new Logger(CronService.name);

    constructor(@InjectModel('Hit') private readonly hitModel: Model<Hit>, private hitService: HitService) {}

    @Cron(CronExpression.EVERY_HOUR)
    async runEveryHour(): Promise<void> {
        const uniqueHits = await this.hitService.getHitsByUrl("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");
        
        uniqueHits.forEach(async (hit: any) => {
            const foundHit = await this.hitModel.find({url: hit.story_url})
            if(foundHit.length === 0) {
                this.logger.debug("NOT FOUNDED");
                
                const newHit = new this.hitModel({
                    url: hit.story_url,
                    date: hit.created_at,
                    title: hit.title ?  hit.title : hit.story_title,
                    author: hit.author,
                    deleted: false
                });
                
                const result = await newHit.save();
                this.logger.debug(result.id);
                
            } else {
                this.logger.debug("FOUNDED");
                this.logger.debug(foundHit);
            }
        });

    }



}
